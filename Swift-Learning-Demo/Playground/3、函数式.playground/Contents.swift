import UIKit

var greeting = "Hello, playground"

// MARK:map
// map 可以依次处理数组中元素，并返回一个处理后的新数组。

let a1 = ["a", "b", "c"]
let a2 = a1.map {
    "\($0)2"
}
print(a2) // ["a2", "b2", "c2"]

//使用 compactMap 可以过滤 nil 的元素。flatMap 会将多个数组合成一个数组返回。

// MARK:filter
// 根据指定条件返回

let a1_1 = ["a", "b", "c", "call my name"]
let a2_1 = a1_1.filter {
    $0.prefix(1) == "c"
}
print(a2_1) // ["c", "call my name"]

// MARK:reduce
// reduce 可以将迭代中返回的结果用于下个迭代中，并，还能让你设个初始值。
let a1_2 = ["a", "b", "c", "call my name.", "get it?"]
let a2_2 = a1_2.reduce("Hey u,", { partialResult, s in
    // partialResult 是前面返回的值，s 是遍历到当前的值
    partialResult + " \(s)"
})

print(a2_2) // Hey u, a b c call my name. get it?

// MARK:sorted
// 排序
// 类型遵循 Comparable
let a1_3 = ["a", "b", "c", "call my name.", "get it?"]
let a2_3 = a1_3.sorted()
let a3_3 = a1_3.sorted(by: >)
let a4_3 = a1_3.sorted(by: <)

print(a2_3) // ["a", "b", "c", "call my name.", "get it?"]
print(a3_3) // ["get it?", "call my name.", "c", "b", "a"]
print(a4_3) // ["a", "b", "c", "call my name.", "get it?"]

// 类型不遵循 Comparable
struct S {
    var s: String
    var i: Int
}
let a5 = [S(s: "a", i: 0), S(s: "b", i: 1), S(s: "c", i: 2)]
let a6 = a5
    .sorted { l, r in
        l.i > r.i
    }
    .map {
        $0.i
    }

print(a6) // [2, 1, 0]

