//
//  LeetCodeListView.swift
//  Swift-Learning-Demo
//
//  Created by yuan xiaodong on 2020/12/4.
//  Copyright © 2020 yuan. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import ObjectMapper

private let cellIdentifier = "cellIdentifier"

class LeetCodeViewController: BaseViewController{
    
    var didSetupConstraints = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "LeetCode"
        view.addSubview(self.tableView)
        view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if !didSetupConstraints {
            tableView.snp_makeConstraints { (maker) in
                maker.edges.equalToSuperview()
            }
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    
    // MARK: - *********** 懒加载 ***********
    lazy var dataArr : NSArray = {
        let jsonData = dataForJson(fileName: "leetCode.json")
        let jsonStr = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)
        let dicArr = getArrayFromJSONString(jsonString: jsonStr! as String)
        
        var sectionArr = [YSListSectionModel]()
        for var dic:[String:Any] in dicArr as! Array {
            let section =  Mapper<YSListSectionModel>().map(JSON: dic)
            sectionArr.append(section!)
        }
        
        return sectionArr as NSArray
    }()
    
    lazy var tableView : UITableView = {
        let table = UITableView.init()
        table.delegate = self;
        table.dataSource = self;
        table.tableFooterView = UIView()
        return table
    }()
    
    // MARK: - *********** 二叉树相关 ***********
    @objc func inorderTraversal(){
        let node = TreeNode.init(1)
        let node1 = TreeNode.init(2)
        let node2 = TreeNode.init(3)
        node1.left = node2;
        node.right = node1;
    
        let arr = TreeSolution().inorderTraversal(node)
        print("【94】二叉树的中序遍历：\(arr)");
    }
    
    @objc func generateTrees(){
        let arr = TreeSolution().generateTrees(3)
        print("【95】不同的二叉搜索树 II：\(arr)");
    }
    
    @objc func numTrees(){
        let count = TreeSolution().numTrees(3)
        print("【96】不同的二叉搜索树：\(count)");
    }
    
    @objc func isValidBST(){
        let node = TreeNode.init(2)
        let node1 = TreeNode.init(1)
        let node2 = TreeNode.init(3)
        node.left = node1;
        node.right = node2;
        let result = TreeSolution().isValidBST_2(node);
        print("【98】验证二叉搜索树：\(result ? "是" : "否")");
    }
    
    @objc func recoverTree(){
        //[5,3,9,-2147483648,2]
        let node = TreeNode.init(5)
        let node1 = TreeNode.init(3)
        let node11 = TreeNode.init(-2147483648)
        let node12 = TreeNode.init(2)
        node1.left = node11;
        node1.right = node12;
        
        let node2 = TreeNode.init(9)
        node.left = node1;
        node.right = node2;
        TreeSolution().recoverTree(node);
        
        print("【99】恢复二叉搜索树：\(node)");
    }
    
    @objc func isSameTree(){
        let node = TreeNode.init(2)
        let node1 = TreeNode.init(1)
        let node2 = TreeNode.init(3)
        node.left = node1;
        node.right = node2;
        
        let node4 = TreeNode.init(2)
        let node5 = TreeNode.init(5)
        node4.left = node5;
        
        let isSame = TreeSolution().isSameTree(node, node4);
        print("【100】相同的树：\(isSame ? "相同" : "不相同")");
    }
    
    @objc func isSymmetric(){
        let node = TreeNode.init(1)
        let node1 = TreeNode.init(2)
        let node1_1 = TreeNode.init(3)
        let node1_2 = TreeNode.init(4)
        node1.left = node1_1;
        node1.right = node1_2;
        node.left = node1
        
        let node2 = TreeNode.init(2)
        let node21 = TreeNode.init(4)
        let node22 = TreeNode.init(3)
        node2.left = node21;
        node2.right = node22;
        node.right = node2
        
        let isSym = TreeSolution().isSymmetric(node);
        print("【101】对称二叉树：\(isSym ? "对称" : "非对称")");
    }
    
    @objc func levelOrder(){
        let node = TreeNode.init(1)
        let node1 = TreeNode.init(2)
        let node1_1 = TreeNode.init(3)
        let node1_2 = TreeNode.init(4)
        node1.left = node1_1;
        node1.right = node1_2;
        node.left = node1
        
        let node2 = TreeNode.init(2)
        let node2_1 = TreeNode.init(4)
        let node2_2 = TreeNode.init(3)
        node2.left = node2_1;
        node2.right = node2_2;
        node.right = node2
        
        let isSym = TreeSolution().levelOrder(node);
        print("【102】二叉树层序遍历：\(isSym)");
    }
    
    @objc func zigzagLevelOrder(){
        let node = TreeNode.init(1)
        let node1 = TreeNode.init(2)
        let node1_1 = TreeNode.init(3)
        let node1_2 = TreeNode.init(4)
        node1.left = node1_1;
        node1.right = node1_2;
        node.left = node1
        
        let node2 = TreeNode.init(2)
        let node2_1 = TreeNode.init(4)
        let node2_2 = TreeNode.init(3)
        node2.left = node2_1;
        node2.right = node2_2;
        node.right = node2
        
        let isSym = TreeSolution().zigzagLevelOrder(node);
        print("【103】二叉树的锯齿形层序遍历：\(isSym)");
    }
    //root = [3,9,20,null,null,15,7]
    
    
    // MARK: - *********** 数组相关 ***********
    @objc func twoSum(){
        let nums = [2, 7, 11, 15]
        let taget = 9
        let arr = ArraySolution().twoSum(nums, taget)
        print("【1】两数之和：\(arr)");
    }
    
    @objc func removeDuplicates(){
        var nums = [0,0,1,1,1,2,2,3,3,4]
        let value = ArraySolution().removeDuplicates(&nums)
        print("【26】删除排序数组中的重复项：\(value)");
    }
    
    @objc func removeElement(){
        var nums = [0,0,1,1,1,2,2,3,3,4]
        let val = 1
        let result = ArraySolution().removeElement(&nums, val)
        print("【27】移除元素：\(result)");
    }
    
    @objc func searchInsert(){
        let nums = [1,3,5,6]
        let val = 5
        let result = ArraySolution().searchInsert(nums, val)
        print("【35】搜索插入位置：\(result)");
    }
    
    @objc func merge(){
        var A = [1,2,3,0,0,0], m = 3
        let B = [2,5,6],       n = 3
        ArraySolution().merge(&A, m, B, n)
        print("【面试题 10.01】合并排序的数组：\(A)");
    }
    
    @objc func maxProfit(){
        let A = [7,1,5,3,6,4]
        let value = ArraySolution().maxProfit(A)
        print("【121】买卖股票的最佳时机：\(value)");
    }
    
    @objc func findMedianSortedArrays(){
        let A = [1,3]
        let B = [2]
        let value = ArraySolution().findMedianSortedArrays(A, B)
        print("【4】寻找两个正序数组的中位数：\(value)");
    }
    
    @objc func maxArea(){
        let arr = [1,8,6,2,5,4,8,3,7]
        let area = ArraySolution().maxArea(arr);
        print("【11】盛最多水的容器：\(area)");
    }
    
    // MARK: - *********** 链表相关 ***********
    @objc func getIntersectionNode(){
        
        /**
         输入：intersectVal = 2, listA = [1,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
         输出：Intersected at '2'
         */
        let listA = ListNode(1,ListNode(9,ListNode(1,ListNode(2,ListNode(4)))))
        let listB = ListNode(3,ListNode(2,ListNode(4)))
        let result = ListNodeSolution().getIntersectionNode(listA, listB)
        
        print("【160】相交链表的结果：\(String(describing: result))");
        printListNode(listA)
        printListNode(listB)
        printListNode(result)
    }
    
    // MARK: - *********** 其他 ***********
    @objc func addTwoNumbers(){
        let a = ListNode(4,ListNode(3))
        let oneList = ListNode(2,a)
        
        let b = ListNode(6,ListNode(4))
        let twoList = ListNode(5,b)
        
        let threeList = Solution().addTwoNumbers(oneList,twoList)
        
        print("【2】两数相加的结果：");
        printListNode(threeList)
    }
    
    func printListNode(_ head: ListNode?) {
        if head?.next != nil {
            printListNode(head?.next)
        }
        print(head!.val)
    }
    
}

// MARK: - *********** UITableViewDelegate ***********
extension LeetCodeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let model = self.dataArr[section] as? YSListSectionModel
        return model?.title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let model = self.dataArr[section] as? YSListSectionModel
        return model!.list.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: YSTableViewCell? = nil
    
        if cell != nil {
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? YSTableViewCell
        }else{
            cell = YSTableViewCell.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: cellIdentifier)
        }
        
        let section = self.dataArr[indexPath.section] as? YSListSectionModel
        let model = section?.list[indexPath.row]
        cell?.model = model
        cell?.textLabel?.text = model?.title
        cell?.detailTextLabel?.text = model?.subTitle
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = self.dataArr[indexPath.section] as? YSListSectionModel
        let model = section?.list[indexPath.row]
        let selector = Selector(model!.event)
        
        if self.responds(to:selector) {
            self.perform(selector)
        }else{
            print("\(selector.description)方法不存在！！！")
        }
    }
}
