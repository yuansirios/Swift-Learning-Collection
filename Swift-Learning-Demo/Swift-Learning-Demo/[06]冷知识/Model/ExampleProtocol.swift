//
//  ExampleProtocol.swift
//  Swift-Learning-Demo
//
//  Created by yuan xiaodong on 2022/6/15.
//  Copyright © 2022 yuan. All rights reserved.
//

import Foundation

protocol ExampleProtocol {
    var simpleDescription : String { get }
    mutating func adjust()
}
