//
//  SimpleStruct.swift
//  Swift-Learning-Demo
//
//  Created by yuan xiaodong on 2022/6/15.
//  Copyright © 2022 yuan. All rights reserved.
//

import Foundation

struct SimpleStruct: ExampleProtocol {
    var simpleDescription: String = "A simple structuer"
    mutating func adjust() {
        simpleDescription += "(adjusted)"
    }
}
