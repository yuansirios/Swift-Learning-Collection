//
//  SimpleClass.swift
//  Swift-Learning-Demo
//
//  Created by yuan xiaodong on 2022/6/15.
//  Copyright © 2022 yuan. All rights reserved.
//

import Foundation

class SimpleClass: ExampleProtocol {
    var simpleDescription: String = "A very simple class"
    var anotherProperty: Int = 110
    
    //在class中实现带有mutating方法的接口时，不用mutating进行修饰。因为对于class来说，类的成员变量和方法都是透明的，所以不必使用mutating来进行修饰
    func adjust() {
        simpleDescription += " Now 100% adjusted"
    }
}
