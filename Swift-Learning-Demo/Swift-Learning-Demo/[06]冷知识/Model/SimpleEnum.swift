//
//  SimpleEnum.swift
//  Swift-Learning-Demo
//
//  Created by yuan xiaodong on 2022/6/15.
//  Copyright © 2022 yuan. All rights reserved.
//

import Foundation

enum SimpleEnum: ExampleProtocol {
    
    case First,Second,Third
    var simpleDescription: String {
        get {
            switch self {
            case .First:
                return "first"
            case .Second:
                return "second"
            case .Third:
                return "third"
            }
        }
        
        set {
            simpleDescription = newValue
        }
    }
    
    mutating func adjust() {
        print("simpleDescription = \(simpleDescription)")
    }
}
